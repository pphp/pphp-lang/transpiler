<?php

require_once "vendor/autoload.php";

use PhpParser\Error;
use PhpParser\NodeDumper;
use PhpParser\ParserFactory;
use PhpParser\Lexer;
use PhpParser\Parser\Tokens;
use PhpParser\PrettyPrinter;


$source = file_get_contents('dummy_src.pphp');

$lexer = new PhpParser\Lexer(array(
    'usedAttributes' => array(
        'comments', 'startLine', 'endLine'
    )
));

$parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7,$lexer);

$prettyPrinter = new PrettyPrinter\Standard;

try {
    $ast = $parser->parse($source);
} catch (Error $error) {
    echo "Parse error: {$error->getMessage()}\n";
    return;
}

$dumper = new NodeDumper;
echo $dumper->dump($ast) . "\n";

file_put_contents("dummy_src.php",$prettyPrinter->prettyPrintFile($ast));